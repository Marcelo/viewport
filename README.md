# Viewport

Crea un viewport por cada imagen glu y glut, de modo que no se crucen los objetos, además, añade movimiento de traslación y rotación a cada figura. Para esto hace uso de la librería OpenGl.

Este proyecto contiene 2 carpetas: 

1. La primera carpeta contiene imágenes referenciales del programa.
    ![imagen1](https://framagit.org/Marcelo/viewport/-/raw/master/Im%C3%A1genes/viewport.png)
2. La segunda carpeta contiene los archivos correspondientes al programa:
    

- build
- nbproject
- src/org/jmtipane
- build.xml
- manifest.mf