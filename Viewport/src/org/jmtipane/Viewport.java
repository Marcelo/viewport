package org.jmtipane;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * Taller 4_Viewport
 * @author Marcelo Tip�n
 * 09/08/2020
 * Programa que genera varias viewport, una por cada figura GLU y GLUT
 * Permitiendo adem�s, controlar el movimiento de cada figura por teclado
 */
public class Viewport implements GLEventListener,KeyListener {

    //variables de Opengl
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static float ang;
    static float ancho, alto;
    GLUquadric quad;
    
    //variables de traslaci�n y rotaci�n
    static float angx=0, angy=0, angz=0, cenx=0, ceny=0;
    static  float tx=0,ty=0, tz=0 ,sx=1,sy=1,rx=0,ry=0, rz=0;
    
    /**
     * Crea una ventana y la muestra
     * @param args 
     */
    public static void main(String[] args) {
        Frame frame = new Frame("Viewport");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new Viewport());
        frame.add(canvas);
        frame.setSize(1000, 1000);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
        
        //permite controlar el frame mediante el teclado
        frame.addKeyListener(new Viewport());
    }

    /**Inicializa variables y color de la pantalla
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    /**
     * Define la forma, perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        ancho = width;
        alto = height;
        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        
//        glu.gluPerspective(45.0f, h, 1.0, 20.0);
//        gl.glOrtho(-200, 200, -200, 200, -200, 200);
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Se dibuja un viewport por cada figura
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL();
        glu = new GLU();
        glut = new GLUT();
        quad = glu.gluNewQuadric();
        
        //Permite usar figuras glu
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        //variable para controlar el tama�o de cada viewport
        float aux=alto/6;
        
        //permite usar transparencias
        gl.glEnable(GL.GL_DEPTH_TEST);

        //Dibujar cinlindro
        gl.glViewport(0, 0, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 0);
        glu.gluCylinder(quad, 10, 10, 20, 10, 10);
        
        //Dibuja un disco
        gl.glViewport(0, (int)aux, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 2, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 0);
        glu.gluDisk(quad, 5, 10, 60, 50);
        
        //Dibuja un disco parcial
        gl.glViewport(0, (int)aux*2, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 2, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 0, 1);
        glu.gluPartialDisk(quad, 1, 10, 60, 50, 10, 20);
        
        //Dibuja una esfera
        gl.glViewport(0, (int)aux*3, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 0);
        glu.gluSphere(quad, 15, 50, 20);
        
        //Dibuja un cono s�lido
        gl.glViewport(0, (int)aux*4, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 2, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 1);
        glut.glutSolidCone(10, 15, 50, 30);
        
        //Dibuja un cubo s�lido
        gl.glViewport(0, (int)aux*5, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 2, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 1);
        glut.glutSolidCube(20);
        
        //Dibuja un cilindro s�lido
        gl.glViewport((int)aux, 0, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 2, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 0);
        glut.glutSolidCylinder(10,15,50,60);
        
        //Dibuja un dodecaedro solido
        gl.glViewport((int)aux, (int)aux, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(10, 10, 10);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 0);
        glut.glutSolidDodecahedron();
        
        //Dibuja un icosaedro solido
        gl.glViewport((int)aux, (int)aux*2, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(15, 15, 15);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 0, 1);
        glut.glutSolidIcosahedron();
        
        //Dibuja un octaedro solido
        gl.glViewport((int)aux, (int)aux*3, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(15, 15, 15);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 0);
        glut.glutSolidOctahedron();
        
        //Dibuja un rombo dodecaedro solido
        gl.glViewport((int)aux, (int)aux*4, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(15, 15, 15);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 1);
        glut.glutSolidRhombicDodecahedron();
        
        //Dibuja una esfera solido
        gl.glViewport((int)aux, (int)aux*5, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScaled(20, 20, 20);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 1);
        glut.glutSolidSphere(1, 20, 35);
        
        //Dibuja una tetera solido
        gl.glViewport((int)aux*2, 0, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(2, 2, 2);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 0);
        glut.glutSolidTeapot(10);
        
        //Dibuja un toroide solido
        gl.glViewport((int)aux*2, (int)aux, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 0);
        glut.glutSolidTorus(8, 12, 50, 50);
        
        //Dibuja un tetraedro solido
        gl.glViewport((int)aux*2, (int)aux*2, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(20, 20, 20);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 0, 1);
        glut.glutSolidTetrahedron();
        
        //Dibuja las lineas de un dodecaedro
        gl.glViewport((int)aux*2, (int)aux*3, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(11, 11, 11);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 0);
        glut.glutWireDodecahedron();
        
        //Dibuja las lineas de un icosaedro
        gl.glViewport((int)aux*2, (int)aux*4, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(20, 20, 20);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 1);
        glut.glutWireIcosahedron();
        
        //Dibuja las lineas de un octaedro
        gl.glViewport((int)aux*2, (int)aux*5, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(20, 20, 20);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 1);
        glut.glutWireOctahedron();
        
        //Dibuja las lineas de un rombo dodecaedro
        gl.glViewport((int)aux*3, (int)aux*0, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(20, 20, 20);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 0);
        glut.glutWireRhombicDodecahedron();
        
        //Dibuja las lineas de una esfera
        gl.glViewport((int)aux*3, (int)aux*1, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 0);
        glut.glutWireSphere(20, 10, 10);
        
        //Dibuja las lineas de una tetera
        gl.glViewport((int)aux*3, (int)aux*2, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 0, 1);
        glut.glutWireTeapot(20);
        
        //Dibuja las lineas de un tetraedro
        gl.glViewport((int)aux*3, (int)aux*3, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(25, 25, 25);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 0);
        glut.glutWireTetrahedron();
        
        //Dibuja las lineas de un toroide
        gl.glViewport((int)aux*3, (int)aux*4, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 0, 1);
        glut.glutWireTorus(5, 20, 15, 15);
        
        //Dibuja las lineas de un cono
        gl.glViewport((int)aux*3, (int)aux*5, (int)aux, (int)aux);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(0, 1, 1);
        glut.glutWireCone(20, 50, 20, 20);
        
        //Dibuja las lineas de un cubo
        gl.glViewport((int)aux*4, (int)aux*0, (int)aux*2, (int)aux*2);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 1);
        glut.glutWireCube(30);
        
        //Dibuja las lineas de un cilindro
        gl.glViewport((int)aux*4, (int)aux*2, (int)aux*2, (int)aux*2);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 1);
        glut.glutWireCylinder(10, 30, 10, 10);
        
        //Dibuja las lineas de un toroide
        gl.glViewport((int)aux*4, (int)aux*4, (int)aux*2, (int)aux*2);
        gl.glLoadIdentity();
        gl.glOrtho(-20, 20, -20, 20, -20, 20);
        gl.glTranslatef(tx, ty, tz);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 1);
        glut.glutWireTorus(5, 20, 15, 15);
        
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Controla los movimientos de cada cuerpo por teclado
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
        tx+=1.0f;
    }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
        tx-=1.0f;
        }
        if(e.getKeyCode() == KeyEvent.VK_UP){
        ty+=1.0f;
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
        ty-=1.0f;}
        
        if(e.getKeyCode() == KeyEvent.VK_1){
        tz-=1.0f;
        }
        if(e.getKeyCode() == KeyEvent.VK_2){
        tz+=1.0f;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_J){
        rx=1f;
        angx=angx+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_L){
        rx=1f;
        angx=angx-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_N){
        rz=1f;
        angz=angz+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_M){
        rz=1f;
        angz=angz-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_R){
        angx=0; angy=0; angz=0; cenx=0; ceny=0;
        tx=0;ty=0; sx=1; sy=1; rx=0; ry=0; rz=0;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

